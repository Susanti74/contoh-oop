<?php

    require('animal.php'); 
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->legs . "<br>";
    echo "cold blooded : " . $sheep->cold_blooded . "<br><br>" ;

    $kodok = new Frog("Buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs. "<br>";
    echo "cold blooded : " .  $kodok->cold_blooded . "<br>";
    echo $kodok->Lompat(). "<br><br>"; 

    $Sungokong = new Ape("Kera Sakti");
    echo "Name : " . $Sungokong->name . "<br>";
    echo "Legs : " . $Sungokong->legs. "<br>";
    echo "cold blooded : " .  $Sungokong->cold_blooded . "<br>";
    echo $Sungokong->Yell(). "<br>"; 




?>